# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Remote::TextingBetaAdapter do
  subject do
    VCR.use_cassette(cassette) do
      described_class.send_message(phone_number: phone_number, text: text)
    end
  end

  describe '.send_message' do
    let(:cassette)           { 'beta_adapter/success' }

    let(:phone_number)       { '123456' }
    let(:text)               { 'test-text' }
    let(:expected_remote_id) { '1234' }

    it 'sends message and return struct with remote message id' do
      expect(subject).to eq(Remote::ResponseStruct.new(message_id: expected_remote_id))
    end

    context 'when adapter is unavailable' do
      let(:cassette) { 'beta_adapter/failure' }

      let(:text)         { 'test-text' }
      let(:phone_number) { '123456' }

      it 'raises error' do
        expect { subject }.to raise_error HTTParty::Error, 'Internal server error'
      end
    end
  end
end
