# frozen_string_literal: true

module TestHelpers
  module SubjectTestHelper
    def error_free_subject
      subject
    rescue ::Errors::UnprocessableError, HTTParty::Error
      nil
    end
  end
end
