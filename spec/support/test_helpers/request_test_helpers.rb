# frozen_string_literal: true

module TestHelpers
  module RequestTestHelpers
    def parsed_response
      JSON.parse(response.body)&.deep_symbolize_keys
    end

    def data
      parsed_response['data']
    end

    def errors
      parsed_response['errors']
    end
  end
end
