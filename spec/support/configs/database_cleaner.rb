# frozen_string_literal: true

require 'database_cleaner/active_record'
require 'database_cleaner/redis'

RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner[:active_record].strategy = :transaction
    DatabaseCleaner[:redis].strategy = :deletion
    DatabaseCleaner[:active_record].clean_with(:truncation)
  end

  config.around do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
