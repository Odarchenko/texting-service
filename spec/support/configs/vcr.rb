# frozen_string_literal: true

require 'vcr'

VCR_CASSETTES_DIR = 'spec/fixtures/vcr_cassettes'

VCR.configure do |config|
  config.cassette_library_dir = VCR_CASSETTES_DIR
  config.hook_into :webmock
  config.configure_rspec_metadata!
  config.ignore_localhost = true
end
