# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SelectTextingAdapterService do
  subject { described_class.new.process }

  let(:first_adapter_name)  { SelectTextingAdapterService::ALPHA_ADAPTER_CLASS_NAME }
  let(:second_adapter_name) { SelectTextingAdapterService::BETA_ADAPTER_CLASS_NAME }

  describe '.select_texting_adapter' do
    it 'returns first texting adapter name' do
      expect(subject).to eq(first_adapter_name)
    end

    context 'when first adapter has more than available percentage of requests' do
      before { Rails.cache.redis.incr(first_adapter_name) }

      it 'returns second adapter name' do
        expect(subject).to eq(second_adapter_name)
      end
    end

    context 'when first adapter is excluded but under percentage' do
      subject { described_class.new(except_adapter_name: first_adapter_name).process }

      it 'returns second adapter name' do
        expect(subject).to eq(second_adapter_name)
      end
    end

    context 'when second adapter is excluded but first is over percentage' do
      subject { described_class.new(except_adapter_name: second_adapter_name).process }

      before { Rails.cache.redis.incr(first_adapter_name) }

      it 'returns second adapter name' do
        expect(subject).to eq(first_adapter_name)
      end
    end
  end
end
