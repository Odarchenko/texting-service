# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UpdateMessageService do
  describe '.process' do
    subject { described_class.new(params: params).process }

    let(:params)     { { message_id: message_id, status: status } }
    let(:message_id) { message.remote_message_id }
    let(:status)     { Message::DELIVERED }

    let(:message) { create(:message, status: Message::SENT) }

    it 'updates message status' do
      subject

      expect(message.reload.status).to eq(Message::DELIVERED)
    end

    it 'returns true' do
      expect(subject).to eq true
    end

    context 'when invalid message_id passed' do
      let(:message_id) { 'invalid' }

      it 'raises error' do
        expect { subject }.to raise_error ::Errors::UnprocessableError
      end

      it 'does not change message status' do
        expect { error_free_subject }.not_to change(message.reload, :status)
      end
    end

    context 'when invalid status passed' do
      let(:status) { 'invalid-status' }

      it 'raises error' do
        expect { subject }.to raise_error ::Errors::UnprocessableError
      end

      it 'does not change message status' do
        expect { error_free_subject }.not_to change(message.reload, :status)
      end
    end
  end
end
