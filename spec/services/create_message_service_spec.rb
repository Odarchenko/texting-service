# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateMessageService do
  subject do
    VCR.use_cassette(cassette_alpha) do
      VCR.use_cassette(cassette_beta) do
        described_class.create_and_send_message(params: params)
      end
    end
  end

  let(:params) { { phone_number: '123456', text: 'test-text' } }

  let(:cassette_alpha) { 'alpha_adapter/success' }
  let(:cassette_beta)  { 'beta_adapter/success' }

  let(:created_message) { Message.last }

  describe '.create_and_send_message' do
    it 'creates message' do
      expect { subject }.to change(Message, :count).by(1)
    end

    it 'returns created message' do
      expect(subject).to eq created_message
    end

    it 'creates message with given params' do
      subject
      expect(created_message).to have_attributes(
        phone_number:      params[:phone_number],
        text:              params[:text],
        remote_message_id: '1234',
        status:            Message::SENT
      )
    end

    it 'sends message' do
      allow(SendMessageService).to receive(:send_message).and_call_original
      subject
      expect(SendMessageService)
        .to have_received(:send_message)
        .with(phone_number: params[:phone_number], text: params[:text])
    end

    context 'when try to pass phone number which has invalid messages' do
      before { create(:message, phone_number: params[:phone_number], status: Message::INVALID) }

      it 'raises error' do
        expect { subject }.to raise_error Errors::UnprocessableError, 'Invalid phone number'
      end

      it 'does not create message' do
        expect { error_free_subject }.not_to change(Message, :count)
      end

      it 'does not send message' do
        allow(SendMessageService).to receive(:send_message).and_call_original
        error_free_subject
        expect(SendMessageService).not_to have_received(:send_message)
      end
    end

    context 'when both of adapters does not work' do
      let(:cassette_alpha) { 'alpha_adapter/failure' }
      let(:cassette_beta)  { 'beta_adapter/failure' }

      it 'creates message with failed status and without remote message_id' do
        error_free_subject
        expect(created_message).to have_attributes(
          phone_number:      params[:phone_number],
          text:              params[:text],
          remote_message_id: nil,
          status:            Message::FAILED
        )
      end

      it 'try to send message' do
        allow(SendMessageService).to receive(:send_message).and_call_original
        error_free_subject
        expect(SendMessageService)
          .to have_received(:send_message)
          .with(phone_number: params[:phone_number], text: params[:text])
      end

      it 'raises error' do
        expect { subject }.to raise_error Errors::UnprocessableError, 'Service temporary unavailable'
      end
    end

    context 'when missing text' do
      let(:params) { { phone_number: '123456' } }

      it 'raises error' do
        expect { subject }.to raise_error Errors::UnprocessableError, 'Invalid message text'
      end

      it 'does not create message' do
        expect { error_free_subject }.not_to change(Message, :count)
      end

      it 'does not send message' do
        allow(SendMessageService).to receive(:send_message).and_call_original
        error_free_subject
        expect(SendMessageService).not_to have_received(:send_message)
      end
    end

    context 'when missing phone_number' do
      let(:params) { { text: 'test' } }

      it 'raises error' do
        expect { subject }.to raise_error Errors::UnprocessableError, 'Invalid phone number'
      end

      it 'does not create message' do
        expect { error_free_subject }.not_to change(Message, :count)
      end

      it 'does not send message' do
        allow(SendMessageService).to receive(:send_message).and_call_original
        error_free_subject
        expect(SendMessageService).not_to have_received(:send_message)
      end
    end
  end
end
