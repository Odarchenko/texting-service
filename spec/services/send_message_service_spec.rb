# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SendMessageService do
  subject do
    VCR.use_cassette(cassette_alpha) do
      VCR.use_cassette(cassette_beta) do
        described_class.send_message(text: text, phone_number: phone_number)
      end
    end
  end

  let(:phone_number) { '123456' }
  let(:text)         { 'test-text' }

  let(:cassette_alpha) { 'alpha_adapter/success' }
  let(:cassette_beta)  { 'beta_adapter/success' }

  let(:expected_remote_message_id) { '1234' }

  describe '.send_message' do
    it 'returns remote_message_id' do
      expect(subject).to eq expected_remote_message_id
    end

    it 'sends notification via alpha adapter' do
      allow(Remote::TextingAlphaAdapter).to receive(:send_message).and_call_original
      subject
      expect(Remote::TextingAlphaAdapter)
        .to have_received(:send_message)
        .with(phone_number: phone_number, text: text)
    end

    it 'increased request counter' do
      subject
      expect(Rails.cache.redis.get(Remote::TextingAlphaAdapter).to_i).to eq 1
    end

    context 'when one of adapter failed' do
      let(:cassette_alpha) { 'alpha_adapter/failure' }

      it 'returns remote_message_id' do
        expect(subject).to eq expected_remote_message_id
      end

      it 'try to sends notification to first adapter' do
        allow(Remote::TextingAlphaAdapter).to receive(:send_message).and_call_original
        subject
        expect(Remote::TextingAlphaAdapter)
          .to have_received(:send_message)
          .with(phone_number: phone_number, text: text)
      end

      it 'increased request counter for all adapters' do
        subject
        expect(Rails.cache.redis.get(Remote::TextingAlphaAdapter).to_i).to eq 1
        expect(Rails.cache.redis.get(Remote::TextingBetaAdapter).to_i).to eq 1
      end

      it 'sends notification to second adapter' do
        allow(Remote::TextingBetaAdapter).to receive(:send_message).and_call_original
        subject
        expect(Remote::TextingBetaAdapter)
          .to have_received(:send_message)
          .with(phone_number: phone_number, text: text)
      end
    end

    context 'when both of adapters raise an error' do
      let(:cassette_alpha) { 'alpha_adapter/failure' }
      let(:cassette_beta)  { 'beta_adapter/failure' }

      it 'try to sends notification to first adapter' do
        allow(Remote::TextingAlphaAdapter).to receive(:send_message).and_call_original
        error_free_subject
        expect(Remote::TextingAlphaAdapter)
          .to have_received(:send_message)
          .with(phone_number: phone_number, text: text)
      end

      it 'increased request counter for all adapters' do
        error_free_subject
        expect(Rails.cache.redis.get(Remote::TextingAlphaAdapter).to_i).to eq 1
        expect(Rails.cache.redis.get(Remote::TextingBetaAdapter).to_i).to eq 1
      end

      it 'try to sends notification to second adapter' do
        allow(Remote::TextingBetaAdapter).to receive(:send_message).and_call_original
        error_free_subject
        expect(Remote::TextingBetaAdapter)
          .to have_received(:send_message)
          .with(phone_number: phone_number, text: text)
      end

      it 'raises error' do
        expect { subject }.to raise_error HTTParty::Error, 'Internal server error'
      end
    end
  end
end
