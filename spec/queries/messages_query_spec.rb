# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MessagesQuery do
  describe '.find_messages' do
    subject { described_class.find_messages(params: params) }

    let(:params)     { { phone_number: message_a.phone_number } }

    let!(:message_a) { create(:message, phone_number: '123') }
    let!(:message_b) { create(:message, phone_number: '234') }

    it 'returns filtered messages' do
      expect(subject).to     include(message_a)
      expect(subject).not_to include(message_b)
    end

    context 'when no params passed' do
      let(:params) { {} }

      it 'returns all messages' do
        expect(subject).to include(message_a, message_b)
      end
    end

    context 'when invalid phone_number passed' do
      let(:params) { { phone_number: 'invalid' } }

      it 'returns empty array' do
        expect(subject).to eq []
      end
    end
  end
end
