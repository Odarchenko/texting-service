# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'MessagesController#index' do # rubocop:disable RSpec/DescribeClass
  subject { page }

  let!(:message_a) { create(:message, status: Message::DELIVERED) }
  let!(:message_b) { create(:message, status: Message::INVALID) }

  before { visit '/' }

  it 'success' do
    expect(subject).to have_current_path '/'

    [message_a, message_b].each do |message|
      within "#messages-#{message.id}" do
        has_text?(message.id)
        has_text?(message.text)
        has_text?(message.phone_number)
        has_text?(message.status.humanize)
      end
    end
  end

  context 'when inputted filter data' do
    it 'filter collection' do
      fill_in 'phone_number', with: message_a.phone_number

      click_button 'Search'

      within "#messages-#{message_a.id}" do
        has_text?(message_a.id)
        has_text?(message_a.text)
        has_text?(message_a.phone_number)
        has_text?(message_a.status.humanize)
      end

      has_no_text?(message_b.id)
    end
  end
end
