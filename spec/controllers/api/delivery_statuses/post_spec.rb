# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::DeliveryStatuses, type: :request do
  describe 'POST /create' do
    let(:params) do
      {
        "message_id": '123456',
        "status":     Message::DELIVERED
      }
    end

    let!(:message) { create(:message, remote_message_id: '123456') }

    before do
      post '/api/v1/delivery_statuses', params: params.to_json, headers: { 'CONTENT_TYPE' => 'application/json' }
    end

    context 'when success' do
      it 'returns success status' do
        expect(response.status).to eq 201
      end

      it 'updates message' do
        expect(message.reload.status).to eq Message::DELIVERED
      end

      it 'returns success true' do
        expect(parsed_response).to eq({ success: true })
      end
    end

    context 'when passed invalid message_id' do
      let(:params) do
        {
          "message_id": '12345',
          "status":     Message::DELIVERED
        }
      end

      it 'returns success status' do
        expect(response.status).to eq 400
      end

      it 'does not updates message' do
        expect(message.reload.status).to eq Message::SENT
      end

      it 'returns success true' do
        expect(parsed_response).to eq({ errors: ['Invalid message_id passed'] })
      end
    end

    context 'when passed invalid status' do
      let(:params) do
        {
          "message_id": '123456',
          "status":     'invalid-status'
        }
      end

      it 'returns success status' do
        expect(response.status).to eq 400
      end

      it 'returns success true' do
        expect(parsed_response).to eq({ errors: ['status does not have a valid value'] })
      end
    end

    context 'when empty params passed' do
      let(:params) { {} }
      let(:errors) do
        [
          'status is missing',
          'status is empty',
          'status does not have a valid value',
          'message_id is missing',
          'message_id is empty'
        ]
      end

      it 'returns success status' do
        expect(response.status).to eq 400
      end

      it 'returns success true' do
        expect(parsed_response).to eq({ errors: errors })
      end
    end
  end
end
