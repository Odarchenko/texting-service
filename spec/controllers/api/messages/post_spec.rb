# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::Messages, type: :request do
  describe 'POST /create' do
    let(:params) do
      {
        "phone_number": '123456',
        "text":         'test'
      }
    end

    def make_request
      post '/api/v1/messages', params: params.to_json, headers: { 'CONTENT_TYPE' => 'application/json' }
    end

    context 'with valid params' do
      before do
        allow(SendMessageService).to receive(:send_message).and_return('1234')
        make_request
      end

      it 'returns created status' do
        expect(response.status).to eq 201
      end

      it 'creates message' do
        expect(Message.count).to eq 1
      end

      it 'returns success true' do
        expect(parsed_response).to eq({ success: true })
      end
    end

    context 'when missing params in data' do
      let(:params) { {} }

      before { make_request }

      it 'returns error status' do
        expect(response.status).to eq 400
      end

      it 'does not creates message' do
        expect(Message.count).to eq 0
      end

      it 'returns error response' do
        expect(parsed_response).to eq({ errors: ['phone_number is missing', 'text is missing'] })
      end
    end

    context 'when adapter does not works' do
      before do
        allow(SendMessageService).to receive(:send_message).and_raise(HTTParty::Error,
                                                                      'Service temporary unavailable')
        make_request
      end

      it 'returns error status' do
        expect(response.status).to eq 400
      end

      it 'does not creates message' do
        expect(Message.count).to eq 1
      end

      it 'returns error response' do
        expect(parsed_response).to eq({ errors: ['Service temporary unavailable'] })
      end
    end

    context 'when phone number already have invalid messages' do
      before do
        create(:message, phone_number: '123456', status: Message::INVALID)
        make_request
      end

      it 'returns error status' do
        expect(response.status).to eq 400
      end

      it 'does not creates message' do
        expect(Message.count).to eq 1
      end

      it 'returns error response' do
        expect(parsed_response).to eq({ errors: ['Invalid phone number'] })
      end
    end
  end
end
