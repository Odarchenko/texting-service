# frozen_string_literal: true

FactoryBot.define do
  factory :message, class: Message.name do
    remote_message_id { rand(10_00..100_00) }
    text              { 'text message' }
    phone_number      { '+12125551234' }
    status            { Message::SENT }
  end
end
