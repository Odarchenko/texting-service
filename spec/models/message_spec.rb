# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Message do
  describe 'validations' do
    subject { build(:message) }

    it { is_expected.to be_valid }
    it { is_expected.to validate_presence_of(:phone_number) }
    it { is_expected.to validate_presence_of(:text) }
    it { is_expected.to validate_inclusion_of(:status).in_array(Message::STATUSES) }
    it { is_expected.to validate_presence_of(:status) }
  end
end
