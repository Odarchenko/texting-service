# frozen_string_literal: true

class MessagesController < ApplicationController
  def index
    @messages = ::MessagesQuery.find_messages(params: params)
  end
end
