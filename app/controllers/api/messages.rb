# frozen_string_literal: true

module Api
  class Messages < Grape::API
    resource :messages do
      desc 'Send text message.' do
        params phone_number: { in: 'body' }
        params text: { in: 'body' }
      end

      params do
        requires :phone_number, type: String
        requires :text,         type: String
      end

      post do
        CreateMessageService.create_and_send_message(params: params)

        { success: true }
      end
    end
  end
end
