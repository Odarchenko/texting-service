# frozen_string_literal: true

module Api
  class DeliveryStatuses < Grape::API
    resource :delivery_statuses do
      desc 'Update message status.' do
        params status:     { in: 'body' }
        params message_id: { in: 'body' }
      end

      params do
        requires :status,     type: String, allow_blank: false, values: Message::STATUSES
        requires :message_id, type: String, allow_blank: false
      end

      post do
        UpdateMessageService.update_message_status(params: params)

        { success: true }
      end
    end
  end
end
