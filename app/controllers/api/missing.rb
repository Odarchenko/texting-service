# frozen_string_literal: true

module Api
  class Missing < Grape::API
    desc 'All other routes should return not-found', hidden: true
    route :any, '*path' do
      not_found_error!
    end
  end
end
