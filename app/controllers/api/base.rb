# frozen_string_literal: true

module Api
  class Base < Grape::API
    version 'v1', using: :path
    format :json
    prefix 'api'

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error!({ errors: e.full_messages }, 400)
    end

    rescue_from Errors::UnprocessableError do |e|
      error!({ errors: [e.message] }, 400)
    end

    rescue_from :all do |e|
      error!({ errors: [e.message] }, 500)
    end

    mount Api::DeliveryStatuses
    mount Api::Messages
    mount Api::Missing     # should be mount as very last one !!!
  end
end
