# frozen_string_literal: true

class MessagesQuery
  def self.find_messages(args)
    new(**args).process
  end

  def initialize(params: {})
    @params = params
  end

  def process
    scope = Message.all
    scope = scope.where(phone_number: params[:phone_number]) if params[:phone_number]

    scope
  end

  private

  attr_reader :params
end
