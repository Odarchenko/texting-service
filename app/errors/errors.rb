# frozen_string_literal: true

module Errors
  BaseError = Class.new(StandardError)

  class UnprocessableError < BaseError
    attr_reader :details

    def initialize(msg, details: [])
      @details = details
      super(msg)
    end
  end
end
