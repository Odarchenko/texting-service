# frozen_string_literal: true

module Remote
  ResponseStruct = Struct.new(:message_id, keyword_init: true)
end
