# frozen_string_literal: true

module Remote
  class TextingAlphaAdapter
    include Helpers::CallbackUrlHelper

    ADAPTER_URL = 'https://mock-text-provider.parentsquare.com/provider1'

    def self.send_message(args)
      new(**args).call
    end

    def initialize(text:, phone_number:)
      @phone_number = phone_number
      @text         = text
    end

    def call
      response = send_text_message

      ResponseStruct.new(message_id: response['message_id'])
    end

    private

    attr_reader :text, :phone_number

    def send_text_message
      response = HTTParty.post(ADAPTER_URL, body: body.to_json)

      raise_error!(response) if response.code == 500

      response
    end

    def body
      {
        to_number:    phone_number,
        message:      text,
        callback_url: generate_callback_url
      }
    end

    def raise_error!(response)
      raise HTTParty::Error, response['message']
    end
  end
end
