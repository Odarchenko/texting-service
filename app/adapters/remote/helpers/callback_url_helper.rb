# frozen_string_literal: true

module Remote
  module Helpers
    module CallbackUrlHelper
      ENDPOINT_URL = 'api/v1/delivery_statuses'

      def generate_callback_url
        [default_url_protocol, '://', base_url, '/', ENDPOINT_URL].join
      end

      private

      delegate :default_url_protocol, :base_url, to: :config

      def config
        @config ||= Rails.application.config
      end
    end
  end
end
