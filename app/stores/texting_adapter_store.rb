# frozen_string_literal: true

module TextingAdapterStore
  def increment_count_of_requests(adapter_key:)
    redis_client.incr(adapter_key)
  end

  def fetch_request_amount(adapter_key:)
    redis_client.get(adapter_key).to_i
  end

  private

  def redis_client
    @redis_client ||= Rails.cache.redis
  end
end
