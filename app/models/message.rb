# frozen_string_literal: true

class Message < ApplicationRecord
  SENT      = 'sent'
  DELIVERED = 'delivered'
  FAILED    = 'failed'
  INVALID   = 'invalid'
  STATUSES = [SENT, DELIVERED, FAILED, INVALID].freeze

  validates :phone_number, presence:  true
  validates :text,         presence:  true
  validates :status,       presence:  true
  validates :status,       inclusion: { in: STATUSES }
end
