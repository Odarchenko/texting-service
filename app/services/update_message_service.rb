# frozen_string_literal: true

class UpdateMessageService
  def self.update_message_status(args)
    new(**args).process
  end

  def initialize(params:)
    @params = params
  end

  def process
    message.update!(status: params[:status])

    true
  rescue ActiveRecord::RecordInvalid => e
    raise Errors::UnprocessableError, e.message
  end

  private

  attr_reader :params

  def message
    @message ||= Message.find_by(remote_message_id: params[:message_id]) || raise_invalid_message_id_error!
  end

  def raise_invalid_message_id_error!
    raise Errors::UnprocessableError, 'Invalid message_id passed'
  end
end
