# frozen_string_literal: true

class SendMessageService
  include TextingAdapterStore

  def self.send_message(args)
    new(**args).process
  end

  def initialize(text:, phone_number:)
    @text         = text
    @phone_number = phone_number
  end

  def process
    texting_adapter_name = select_texting_adapter
    process_and_sending_message(texting_adapter_name)
  rescue HTTParty::Error
    texting_adapter_name = select_texting_adapter(except_adapter_name: texting_adapter_name)
    process_and_sending_message(texting_adapter_name)
  end

  private

  attr_reader :text, :phone_number

  def process_and_sending_message(texting_adapter_name)
    # decided that if application tried to execute request it meaning that we need to count it
    increment_count_of_requests(adapter_key: texting_adapter_name)

    response = send_message(texting_adapter_name)

    response.message_id
  end

  def send_message(texting_adapter_name)
    texting_adapter_name.constantize.send_message(
      phone_number: phone_number,
      text:         text
    )
  end

  def select_texting_adapter(except_adapter_name: nil)
    SelectTextingAdapterService.new(except_adapter_name: except_adapter_name).process
  end
end
