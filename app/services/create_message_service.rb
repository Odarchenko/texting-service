# frozen_string_literal: true

class CreateMessageService
  include TextingAdapterStore

  def self.create_and_send_message(args)
    new(**args).process
  end

  def initialize(params:)
    @params = params
  end

  def process
    validate_input!

    remote_message_id = send_message
    create_message!(status: Message::SENT, remote_message_id: remote_message_id)
  rescue HTTParty::Error
    create_message!(status: Message::FAILED)

    raise ::Errors::UnprocessableError, 'Service temporary unavailable'
  end

  private

  attr_reader :params

  def send_message
    SendMessageService.send_message(
      phone_number: params[:phone_number],
      text:         params[:text]
    )
  end

  def create_message!(status:, remote_message_id: nil)
    Message.create!(
      phone_number:      params[:phone_number],
      text:              params[:text],
      status:            status,
      remote_message_id: remote_message_id
    )
  end

  def validate_input!
    validate_phone_number!
    validate_text!
  end

  def validate_text!
    text_error! if params[:text].blank?
  end

  def validate_phone_number!
    invalid_phone_number_error! if params[:phone_number].blank? || Message.exists?(status:       Message::INVALID,
                                                                                   phone_number: params[:phone_number])
  end

  def text_error!
    raise ::Errors::UnprocessableError, 'Invalid message text'
  end

  def invalid_phone_number_error!
    raise ::Errors::UnprocessableError, 'Invalid phone number'
  end
end
