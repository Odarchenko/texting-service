# frozen_string_literal: true

class SelectTextingAdapterService
  include TextingAdapterStore

  ALPHA_ADAPTER_CLASS_NAME = 'Remote::TextingAlphaAdapter'
  BETA_ADAPTER_CLASS_NAME  = 'Remote::TextingBetaAdapter'
  ADAPTER_NAMES            = [ALPHA_ADAPTER_CLASS_NAME, BETA_ADAPTER_CLASS_NAME].freeze
  ADAPTER_PERCENTAGE       = {
    ALPHA_ADAPTER_CLASS_NAME => 30,
    BETA_ADAPTER_CLASS_NAME  => 70
  }.freeze

  def initialize(except_adapter_name: nil)
    @except_adapter_name = except_adapter_name
  end

  def process
    ADAPTER_PERCENTAGE.each do |adapter_key, max_percentage|
      next if adapter_key == except_adapter_name

      return adapter_key if allowed_to_use_adapter?(adapter_key, max_percentage)
    end

    ADAPTER_NAMES.without(except_adapter_name).sample
  end

  private

  attr_reader :except_adapter_name

  def allowed_to_use_adapter?(adapter_key, max_percentage)
    return true if total_amount_of_requests.zero?

    requests_amount        = fetch_request_amount(adapter_key: adapter_key).to_f
    percentage_of_requests = (requests_amount / total_amount_of_requests) * 100

    percentage_of_requests < max_percentage
  end

  def total_amount_of_requests
    @total_amount_of_requests ||= ADAPTER_NAMES.map { |name| fetch_request_amount(adapter_key: name) }.sum
  end
end
