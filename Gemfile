# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.1'

gem 'rails', '~> 7.0.6'

gem 'bootsnap', require: false
gem 'factory_bot_rails', '~> 6.2'
gem 'grape', '~> 1.7'
gem 'httparty', '~> 0.21.0'
gem 'importmap-rails'
gem 'jbuilder'
gem 'pg', '~> 1.1'
gem 'puma', '~> 5.0'
gem 'redis', '~> 4.0'
gem 'semantic-ui-sass', '~> 2.4', '>= 2.4.4.0'
gem 'sprockets-rails'
gem 'stimulus-rails'
gem 'turbo-rails'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

group :development, :test do
  gem 'debug', platforms: %i[mri mingw x64_mingw]
  gem 'pry-rails'
  gem 'rspec-rails', '~> 6.0', '>= 6.0.3'
  gem 'shoulda-matchers', '~> 5.3'
end

group :development do
  gem 'grape_on_rails_routes'
  gem 'rubocop', '= 1.27', require: false
  gem 'rubocop-rails', '~> 2.15', require: false
  gem 'rubocop-rspec', '~> 2.11', require: false
  gem 'web-console'
end

group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'database_cleaner-active_record'
  gem 'database_cleaner-redis', '~> 2.0'
  gem 'selenium-webdriver'
  gem 'vcr', '~> 6.2'
  gem 'webdrivers'
  gem 'webmock', '~> 3.18'
end
