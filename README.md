### Before start be sure that you populated default_url_protocol and base url in config
Link to demo: https://intense-headland-04005-7399f55cfa39.herokuapp.com/
### Setup database
input your secrets for example secrets.example.yml
```sh 
$ bundle install
$ rake db:create
$ rake db:migrate
```
### Run app
``` sh
 rails s
```

or change your data in db/seeds.rb and run rake db:seed
### Run tests
```sh 
$ rspec spec
```
### Run rubocop 
```sh
 rubocop
```


### Endpoint to send message ``api/v1/messages``
### Example of body data in request
```sh 
{
    "phone_number": "1232456",
    "text": "test"
}
```
Next steps: 
1. crate asycn cron job for resend failed messages
2. rethink about load balancer
3. add regex phone_number validation
