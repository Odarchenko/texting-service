# frozen_string_literal: true

class CreateMessages < ActiveRecord::Migration[7.0]
  MESSAGE_STATUS_TYPES = %w[sent delivered failed invalid].freeze

  def change
    create_enum :message_status_types, MESSAGE_STATUS_TYPES

    create_table :messages do |t|
      t.text   :text,              null: false
      t.string :phone_number,      index: true, null: false
      t.string :remote_message_id, index: true
      t.enum   :status,            null: false, default: :sent, enum_type: 'message_status_types'

      t.timestamps
    end
  end
end
